package com.lucaongaro.minhash

import scala.language.implicitConversions

case class Buckets( bands: Map[Int, Hash] ) {

  /* Returns a Tuple2 of only the buckets that changed (first element of the
   * tuple contains the values of this, second contains the values of other) */
  def changes( other: Buckets ) = {
    (bands, other.bands).zipped.filter { case (t, o) => t._2 != o._2 }
  }
}

object Buckets {
  implicit def bucketsToMap( buckets: Buckets ) = buckets.bands
  implicit def mapToBuckets( m: Map[Int, Hash] ) = Buckets( m )
}
