package com.lucaongaro.minhash.store

import com.lucaongaro.minhash._
import scala.concurrent._

trait Store[S] {
  implicit val executionContext: ExecutionContext

  /**
   * Update the signature for the given setId and return the old and
   * the new value
   *
   * This updates the existing signature according to MinHash logic,
   * therefore the resulting signature is not necessary equal to the
   * one passed as a second argument.
   */
  def updateSignature( setId: S, signature: Signature ): Future[(Option[Signature], Signature)]

  /**
   * Get the signature for the given setId, if present
   */
  def getSignature( setId: S ): Future[Option[Signature]]

  /**
   * Get the signature for all setIds in the given collection
   *
   * The resulting map only contains entres for setIds for which
   * the signature was found
   */
  def getSignatures( setIds: Seq[S] ): Future[Map[S, Signature]] = {
    val setIdList = setIds.toList
    for {
      signatures <- Future.traverse( setIdList )( getSignature )
    } yield ( setIdList zip signatures ).filter( _._2 != None ).map( t => (t._1, t._2.get ) ).toMap
  }

  /**
   * Put the given setId in the given bands/buckets
   */
  def putInBuckets( buckets: Map[Int, Hash], setId: S ): Future[Unit]

  /**
   * Remove the given setId from the given bands/buckets
   */
  def removeFromBuckets( buckets: Map[Int, Hash], setId: S ): Future[Unit]

  /**
   * Get the setIds in the given bands/buckets
   */
  def getInBuckets( buckets: Map[Int, Hash] ): Future[Set[S]]

  /**
   * Retrieve the stored seeds for the hashing functions,
   * or stores and return the given ones if there is none
   */
  def getSeedsOrElseUpdate( seeds: => Seq[Int] ): Seq[Int]
}
